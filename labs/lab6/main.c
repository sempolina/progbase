#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <progbase.h>
#include <progbase/console.h>
#include <progbase/canvas.h>
int main()
{
    Console_clear();
    struct ConsoleSize consolesize = Console_size();
    int widthPixels = consolesize.columns;
    int heightPixels = consolesize.rows * 2;
    Canvas_setSize(widthPixels, heightPixels);
    Canvas_invertYOrientation();
    Canvas_beginDraw();
    struct Point
    {
        int x;
        int y;
    };
    struct Point C = {widthPixels / 2, heightPixels / 2};
    struct Point L1l = {0, C.y};
    struct Point L1r = {widthPixels, heightPixels / 2};
    float alpha = 0.1, R = 20;
    struct Point L2r = {widthPixels, ((widthPixels - C.x) * tan(alpha) + C.y)};
    struct Point L2l = {0, ((-C.x) * tan(alpha) + C.y)};
    struct Point A = {widthPixels / 4, C.y};
    Canvas_setColorRGB(150, 0, 0);
    Canvas_fillCircle(A.x, A.y, R);
    Canvas_setColorRGB(0, 150, 100);
    for (int x1 = A.x - R + 2; x1 <= A.x + R; x1++)
    {
        for (int y1 = A.y - R; y1 <= A.y + R; y1++)
        {
            if ((y1 < ((x1 - C.x) * tan(alpha) + C.y)) && (copysign(1, pow((x1 - A.x), 2) + pow((y1 - A.y), 2) - pow(R, 2)) != copysign(1, pow(R, 2))))
            {
                Canvas_putPixel(x1, y1);
            }
        }
    }
    Canvas_setColorRGB(0, 100, 0);
    Canvas_strokeCircle(A.x, A.y, R);
    Canvas_setColorRGB(0, 0, 150);
    Canvas_strokeLine(L1l.x, L1l.y, L1r.x, L1r.y);
    Canvas_strokeLine(L2l.x, L2l.y, L2r.x, L2r.y);
    Canvas_setColorRGB(100, 0, 150);
    Canvas_putPixel(C.x, C.y);
    Canvas_putPixel(A.x, A.y);
    Canvas_endDraw();
    char com=Console_getChar();
    while (com != 'q')
    {
        Console_clear();
        if ((com == 'x') && (C.x > 0))
        {
            C.x = C.x - 2;
            L2r.y = (widthPixels - C.x) * tan(alpha) + C.y;
            L2l.y = (-C.x) * tan(alpha) + C.y;
        }
        else if ((com == 'v') && (C.x < widthPixels))
        {
            C.x = C.x + 2;
            L2r.y = (widthPixels - C.x) * tan(alpha) + C.y;
            L2l.y = (-C.x) * tan(alpha) + C.y;
        }
        else if (com == 'z')
        {
            C.y = C.y - 2;
            L2r.y = (widthPixels - C.x) * tan(alpha) + C.y;
            L2l.y = (-C.x) * tan(alpha) + C.y;
            L1r.y = C.y;
            L1l.y = C.y;
            A.y = C.y;
        }
        else if (com == 'b')
        {
            C.y = C.y + 2;
            L2r.y = (widthPixels - C.x) * tan(alpha) + C.y;
            L2l.y = (-C.x) * tan(alpha) + C.y;
            L1r.y = C.y;
            L1l.y = C.y;
            A.y = C.y;
        }
        else if ((com == 'a') && (A.x - R > 0))
        {
            A.x = A.x - 2;
        }
        else if ((com == 'd') && (A.x + R < widthPixels))
        {
            A.x = A.x + 2;
        }
        else if ((com == 'e') && (R > 2))
        {
            R = R - 2;
        }
        else if (com == 't')
        {
            R = R + 2;
        }
        else if (com == 'k')
        {
            alpha = alpha - 0.1;
            L2r.y = (widthPixels - C.x) * tan(alpha) + C.y;
            L2l.y = (-C.x) * tan(alpha) + C.y;
        }
        else if (com == 'l')
        {
            alpha = alpha + 0.1;
            L2r.y = (widthPixels - C.x) * tan(alpha) + C.y;
            L2l.y = (-C.x) * tan(alpha) + C.y;
        }
        Canvas_beginDraw();
        Canvas_setColorRGB(150, 0, 0);
        Canvas_fillCircle(A.x, A.y, R);
        Canvas_setColorRGB(0, 150, 100);
        for (int x1 = A.x - R + 2; x1 <= A.x + R; x1++)
        {
            for (int y1 = A.y - R; y1 <= A.y + R; y1++)
            {
                if ((y1 < ((x1 - C.x) * tan(alpha) + C.y)) && (copysign(1, pow((x1 - A.x), 2) + pow((y1 - A.y), 2) - pow(R, 2)) != copysign(1, pow(R, 2))))
                {
                    Canvas_putPixel(x1, y1);
                }
            }
        }
        Canvas_setColorRGB(0, 100, 0);
        Canvas_strokeCircle(A.x, A.y, R);
        Canvas_setColorRGB(0, 0, 150);
        Canvas_strokeLine(L1l.x, L1l.y, L1r.x, L1r.y);
        Canvas_strokeLine(L2l.x, L2l.y, L2r.x, L2r.y);
        Canvas_setColorRGB(100, 0, 150);
        Canvas_putPixel(C.x, C.y);
        Canvas_putPixel(A.x, A.y);
        Canvas_endDraw();
        com=Console_getChar();
    }
}