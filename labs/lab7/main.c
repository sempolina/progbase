#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <progbase.h>
#include <progbase/console.h>
#include <progbase/canvas.h>
#include <stdbool.h>
#include <time.h>
struct Point
{
    int x;
    int y;
};
struct Circle
{
    struct Point C;
    int R;
    int speed;
};
void draw_Circles(struct Circle Cir[], int n, float alpha, struct Point C)
{
    for (int k = 0; k < n; k++)
    {
        for (int x1 = Cir[k].C.x - Cir[k].R + 2; x1 <= Cir[k].C.x + Cir[k].R; x1++)
        {
            for (int y1 = Cir[k].C.y - Cir[k].R; y1 <= Cir[k].C.y + Cir[k].R; y1++)
            {
                if ((y1 < ((x1 - C.x) * tan(alpha) + C.y)) && (copysign(1, pow((x1 - Cir[k].C.x), 2) + pow((y1 - Cir[k].C.y), 2) - pow(Cir[k].R, 2)) != copysign(1, pow(Cir[k].R, 2))))
                {
                    Canvas_setColorRGB(0, 150, 100);
                    Canvas_putPixel(x1, y1);
                }
            }
        }
    }
}
void draw(struct Point L1l, struct Point L1r, struct Point L2r, struct Point L2l, struct Point C, float alpha, struct Circle Cir[], int n, int widthPixels, int heightPixels)
{
    Console_clear();
    Canvas_beginDraw();
    Canvas_setColorRGB(150, 0, 0);
    for (int i = 0; i < n; i++)
    {
        Canvas_fillCircle(Cir[i].C.x, Cir[i].C.y, Cir[i].R);
    }
    draw_Circles(Cir, n, alpha, C);
    Canvas_setColorRGB(0, 100, 0);
    for (int i = 0; i < n; i++)
    {
        Canvas_strokeCircle(Cir[i].C.x, Cir[i].C.y, Cir[i].R);
    }
    Canvas_setColorRGB(0, 0, 150);
    Canvas_strokeLine(L1l.x, L1l.y, L1r.x, L1r.y);
    Canvas_strokeLine(L2l.x, L2l.y, L2r.x, L2r.y);
    Canvas_setColorRGB(100, 0, 150);
    Canvas_putPixel(C.x, C.y);
    for (int i = 0; i < n; i++)
    {
        Canvas_putPixel(Cir[i].C.x, Cir[i].C.y);
    }

    Canvas_endDraw();
}
int main()
{
    srand(time(0));
    int n = 5;
    Console_clear();
    struct ConsoleSize consolesize = Console_size();
    int widthPixels = consolesize.columns;
    int heightPixels = consolesize.rows * 2;
    struct Point C = {widthPixels / 2, heightPixels / 2};
    struct Point L1l = {0, C.y};
    struct Point L1r = {widthPixels, heightPixels / 2};
    float alpha = 0.1;
    struct Point L2r = {widthPixels, ((widthPixels - C.x) * tan(alpha) + C.y)};
    struct Point L2l = {0, ((-C.x) * tan(alpha) + C.y)};
    struct Circle Cir[n];
    bool hand[n];
    float FPS = 30;
    int delay = 1000 / FPS;
    float t=FPS/1000;
    int R = 5 * n;
    Cir[0].R = 5;
    Cir[0].C.y = C.y;
    Cir[0].C.x = Cir[0].R;
    hand[0] = true;
    Cir[0].speed=rand()%65+34;
    for (int i = 1; i < n; i++)
    {
        Cir[i].R = rand()%21+5;
        Cir[i].C.y = C.y;
        Cir[i].C.x = Cir[i].R + Cir[i - 1].C.x+Cir[i-1].R;
        hand[i] = true;
        Cir[i].speed=rand()%61+30;
    }
    Canvas_setSize(widthPixels, heightPixels);
    Canvas_invertYOrientation();
    draw(L1l, L1r, L2r, L2l, C, alpha, Cir, n, widthPixels, heightPixels);
   
    while (!Console_isKeyDown())
    {
        for (int i = 0; i < n; i++)
        {
            if (hand[i] == true)
            {
                if (Cir[i].C.x + Cir[i].R >= widthPixels)
                {
                    hand[i] = false;
                    Cir[i].C.x-=Cir[i].speed*t;

                }
                else
                {
                    Cir[i].C.x+=Cir[i].speed*t;
                }
            }
            else
            {
                if (Cir[i].C.x - Cir[i].R <= 0)
                {
                    hand[i] = true;
                    Cir[i].C.x+=Cir[i].speed*t;
                }
                else
                {
                    Cir[i].C.x-=Cir[i].speed*t;
                }
            }
        }
        L2r.y=(widthPixels - C.x) * tan(alpha) + C.y;
        L2l.y=(-C.x) * tan(alpha) + C.y;
        alpha = alpha + 0.05;
        draw(L1l, L1r, L2r, L2l, C, alpha, Cir, n, widthPixels, heightPixels);
        sleepMillis(delay);
    }
}