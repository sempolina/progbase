#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
struct Dyn_int
{
    int capacity;
    int *array;
    int lenght;
};
struct string
{
    int capacity;
    char *array;
    int lenght;
};
struct Dyn_string
{
    int capacity;
    struct string *str;
    int lenght;
};
void dyn_int_init(FILE *fin, struct Dyn_int *p)
{
    p->capacity = 16;
    p->lenght = 0;
    p->array = malloc(p->capacity * sizeof(int));
    if (p->array == NULL)
    {
        printf("Creating new list is impossible\n");
        fclose(fin);
        exit(1);
    }
}
void numbers(FILE *fin, struct Dyn_int *p)
{
    int dig = 0;
    while (fscanf(fin, "%i", &dig) > 0)
    {
        if (p->capacity == p->lenght)
        {
            p->capacity += 16;
            int *new_array = realloc(p->array, p->capacity * sizeof(int));
            if (new_array == NULL)
            {
                printf("Memory reallocation error\n");
                fclose(fin);
                free(p->array);
                exit(1);
            }
            else
            {
                p->array = new_array;
            }
        }
        p->array[p->lenght] = dig;
        p->lenght++;
    }
}
float dyn_aver(struct Dyn_int *p)
{
    float aver = 0;
    int numb = 0;
    for (int i = 0; i < p->lenght; i++)
    {
        aver += p->array[i];
        numb++;
    }
    if (numb == 0)
    {
        return NAN;
    }
    else
        return aver / numb;
}
void dyn_string_init(FILE *fin, struct Dyn_string *p)
{
    p->capacity = 10;
    p->lenght = 0;
    p->str = malloc(p->capacity * sizeof(struct string));
    if (p->str == NULL)
    {
        printf("Creating new list is impossible\n");
        fclose(fin);
        exit(1);
    }
    for (int i = 0; i < p->capacity; i++)
    {
        p->str[i].capacity = 20;
        p->str[i].lenght = 0;
        p->str[i].array = malloc(p->str[i].capacity * sizeof(char));
        if (p->str[i].array == NULL)
        {
            printf("Creating new list is impossible\n");
            for (int j = 0; j < i; j++)
            {
                free(p->str[j].array);
            }
            free(p->str);
            fclose(fin);
            exit(1);
        }
    }
}
void dyn_strings(FILE *fin, struct Dyn_string *p)
{
    int i = 0;
    while (fgets(p->str[i].array, 19, fin) != NULL)
    {
        if (p->capacity - 1 == p->lenght)
        {
            p->capacity = p->capacity + 10;
            struct string *new_array = realloc(p->str, p->capacity * sizeof(struct string));
            if (new_array == NULL)
            {
                printf("Memory reallocation error\n");
                fclose(fin);
                for (int j = 0; j < (p->capacity - 10); j++)
                {
                    free(p->str[j].array);
                }
                free(p->str);
                exit(1);
            }
            else
            {
                p->str = new_array;
                for (int j = p->lenght + 1; j < p->capacity; j++)
                {
                    p->str[j].capacity = 20;
                    p->str[j].lenght = 0;
                    p->str[j].array = malloc(p->str[j].capacity * sizeof(char));
                    if (p->str[j].array == NULL)
                    {
                        printf("Memory reallocation error\n");
                        for (int k = 0; k < j; k++)
                        {
                            free(p->str[k].array);
                        }
                        free(p->str);
                        fclose(fin);
                        exit(1);
                    }
                }
            }
        }
        while (strchr(p->str[i].array, '\n') == NULL)
        {
            p->str[i].capacity = p->str[i].capacity + 20;
            p->str[i].lenght += 19;
            char *n_array = realloc(p->str[i].array, p->str[i].capacity * sizeof(char));
            if (n_array == NULL)
            {
                printf("Memory reallocation error\n");
                fclose(fin);
                for (int j = 0; j < p->capacity; j++)
                {
                    free(p->str[j].array);
                }
                free(p->str);
                fclose(fin);
                exit(1);
            }
            p->str[i].array = n_array;
            char a[30];
            fgets(a, 19, fin);
            strcat(p->str[i].array, a);
        }
        p->str[i].lenght = strlen(p->str[i].array) - 1;
        i++;
        p->lenght++;
    }
}
float dyn_string_average(struct Dyn_string *p)
{
    float aver = 0;
    int numb = 0;
    for (int i = 0; i < p->lenght; i++)
    {
        aver += p->str[i].lenght;
        numb++;
    }
    if (numb == 0)
    {
        return NAN;
    }
    else
        return aver / numb;
}
void dyn_string_output(struct Dyn_string *p)
{
    for (int i = 0; i < p->lenght; i++)
    {
        puts(p->str[i].array);
    }
}
void dyn_int_output(struct Dyn_int *p)
{
    for (int i = 0; i < p->lenght; i++)
    {
        printf("%i ", p->array[i]);
    }
    printf("\n");
}
int main(int argc, char *argv[argc])
{
    if (argc < 3)
    {
        printf("You forgot to enter arguments\n");
        return EXIT_FAILURE;
    }
    FILE *fin = fopen(argv[1], "r");
    if (fin == NULL)
    {
        printf("Input file doesn`t exist\n");
        return EXIT_FAILURE;
    }
    char command[10];
    fgets(command, 10, fin);
    if (strncmp(command, "numbers", 7) == 0 && strlen(command) == 8)
    {
        struct Dyn_int numb;
        dyn_int_init(fin, &numb);
        numbers(fin, &numb);
        dyn_int_output(&numb);
        if (fclose(fin) != 0)
        {
            printf("File can`t be closed\n");
            free(numb.array);
            return EXIT_FAILURE;
        }
        float aver = dyn_aver(&numb);
        if (aver == NAN)
        {
            printf("There are no numbers\n");
            free(numb.array);
            return EXIT_FAILURE;
        }
        FILE *fout = fopen(argv[2], "w");
        for (int i = 0; i < numb.lenght; i++)
        {
            if (numb.array[i] > aver)
                fprintf(fout, "%i ", numb.array[i]);
        }
        if (fclose(fout) != 0)
        {
            free(numb.array);
            printf("File can`t be closed\n");
            return EXIT_FAILURE;
        }
        free(numb.array);
    }
    else if (strncmp(command, "text", 4) == 0 && strlen(command) == 5)
    {
        struct Dyn_string text;
        dyn_string_init(fin, &text);
        dyn_strings(fin, &text);
        dyn_string_output(&text);
        if (fclose(fin) != 0)
        {
            printf("File can`t be closed\n");
            for (int j = 0; j < text.capacity; j++)
            {
                free(text.str[j].array);
            }
            free(text.str);
            return EXIT_FAILURE;
        }
        float aver = dyn_string_average(&text);
        if (aver == NAN)
        {
            printf("There are no strings\n");
            for (int j = 0; j < text.capacity; j++)
            {
                free(text.str[j].array);
            }
            free(text.str);
            return EXIT_FAILURE;
        }
        FILE *fout = fopen(argv[2], "w");
        for (int i = 0; i < text.lenght; i++)
        {
            if (text.str[i].lenght < aver)
                fputs(text.str[i].array,fout);
        }
        for (int j = 0; j < text.capacity; j++)
        {
            free(text.str[j].array);
        }
        free(text.str);
        if (fclose(fout) != 0)
        {
            printf("File can`t be closed\n");
            return EXIT_FAILURE;
        }
    }
    else
    {
        printf("Fitst string should be \"text\" or \"numbers\"\n");
        fclose(fin);
        return EXIT_FAILURE;
    }
}