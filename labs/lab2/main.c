#include <stdio.h>
#include <math.h>
#include <stdbool.h>
int main()
{

  float x=-10,y=0;
  bool a=true;
  while(x<=10)  
  {
    if (x==0)
    {
        a=false;
    }
    else if((x>=-4.5&&x<-1)||(x>1&&x<=4.5))
    {
        y=(tan(x+2))+2;
    }
    else
    {
        y=pow((x+3),3)+1/x;
    }
    if(a==true)  
  {
    printf("y(%.1f) = %f\n",x,y);
  }
  else 
  {
  printf("y(0.0) = ERROR\n");
  a=true;
  }
  x=x+0.5;
  } 
}