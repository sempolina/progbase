#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
struct SLNode
{
    float data;
    struct SLNode *next;
};
struct Dynarray
{
    int capacity;
    int *array;
    int lenght;
};
void slnode_clear(struct SLNode *head)
{
    struct SLNode *node = head;
    while (node != NULL)
    {
        struct SLNode *next = node->next;
        free(node);
        node = next;
        next = NULL;
    }
}
struct SLNode *slnode_create(struct SLNode *head, float data)
{
    struct SLNode *pnode = malloc(sizeof(struct SLNode));
    if (pnode == NULL)
    {
        printf("Memmory error\n");
        slnode_clear(head);
        exit(1);
    }
    else
    {
        pnode->data = data;
        pnode->next = NULL;
        return pnode;
    }
}
struct SLNode *slnode_push_back(struct SLNode *head, float value)
{
    struct SLNode *new_node = slnode_create(head, value);
    if (head == NULL)
    {
        return new_node;
    }
    else
    {
        struct SLNode *node = head;
        while (node->next != NULL)
            node = node->next;
        node->next = new_node;
        return head;
    }
}
struct SLNode *slnode_push_front(struct SLNode *head, float value)
{
    struct SLNode *new_node = slnode_create(head, value);
    new_node->next = head;
    return new_node;
}
float slnode_sum(struct SLNode *head)
{
    float sum = 0;
    bool i = false;
    struct SLNode *node = head;
    while (node != NULL)
    {
        i = true;
        sum = sum + node->data;
        node = node->next;
    }
    if (i == false)
        return NAN;
    return sum;
}
size_t slnode_count_pos(struct SLNode *head)
{
    size_t pos = 0;
    struct SLNode *node = head;
    while (node != NULL)
    {
        if (node->data > 0)
            pos++;
        node = node->next;
    }
    return pos;
}
void slnode_print(struct SLNode *head)
{
    struct SLNode *node = head;
    while (node != NULL)
    {
        printf("%f ", node->data);
        node = node->next;
    }
    printf("\n");
}
size_t slnode_size(struct SLNode *head)
{
    size_t size = 0;
    struct SLNode *node = head;
    while (node != NULL)
    {
        size++;
        node = node->next;
    }
    return size;
}
void dynarray_output(struct Dynarray *p)
{
    for (int i = 0; i < p->lenght; i++)
    {
        printf("%i ", p->array[i]);
    }
    printf("\n");
}
void dynarray_print(struct Dynarray *p)
{
    printf("Capacity:%i\n", p->capacity);
    printf("Lenght:%i\n", p->lenght);
    dynarray_output(p);
}
void dynarray_deinit(struct Dynarray *p)
{
    p->capacity = 0;
    p->lenght = 0;
    free(p->array);
}
void dynarray_clear(struct Dynarray *p)
{
    p->lenght = 0;
}
void dynarray_pop_front(struct Dynarray *p)
{
    for (int i = 0; i < p->lenght - 1; i++)
    {
        p->array[i] = p->array[i + 1];
    }
    if (p->lenght > 0)
        p->lenght--;
}
void dynarray_resize(struct Dynarray *p, int new_lenght, int value)
{

    if (p->capacity <= new_lenght)
    {
        p->capacity = new_lenght * 2;
        int *new_array = realloc(p->array, p->capacity * sizeof(int));
        if (new_array == NULL)
        {
            printf("Memory reallocation error\n");
            free(p->array);
            exit(1);
        }
        else
        {
            p->array = new_array;
        }
    }
    for (int i = p->lenght; i < new_lenght; i++)
    {
        p->array[i] = value;
    }
    p->lenght = new_lenght;
}
void dynarray_insert(struct Dynarray *p, int new_index, int value)
{
    if (p->capacity <= p->lenght + 1)
    {
        p->capacity = p->capacity * 2;
        int *new_array = realloc(p->array, p->capacity * sizeof(int));
        if (new_array == NULL)
        {
            printf("Memory reallocation error\n");
            free(p->array);
            exit(1);
        }
        else
        {
            p->array = new_array;
        }
    }
    for (int i = p->lenght; i > new_index; i--)
    {
        p->array[i] = p->array[i - 1];
    }
    p->array[new_index] = value;
    p->lenght++;
}
void dynarray_init(struct Dynarray *p)
{
    p->capacity = 16;
    p->lenght = 0;
    p->array = malloc(p->capacity * sizeof(int));
    if (p->array == NULL)
    {
        printf("Creating new list is impossible\n");
        exit(1);
    }
}
void dynarray_main()
{
    struct Dynarray list;
    printf("Init:\n");
    dynarray_init(&list);
    dynarray_print(&list);
    printf("Resize:\n1. new lenght 18\n2. value 2\n");
    dynarray_resize(&list, 18, 2);
    dynarray_print(&list);
    printf("Insert:\n1. index 5\n2. value 56\n");
    dynarray_insert(&list, 5, 56);
    dynarray_print(&list);
    printf("pop_front\n");
    dynarray_pop_front(&list);
    dynarray_print(&list);
    printf("Clear:\n");
    dynarray_clear(&list);
    dynarray_print(&list);
    printf("Deinit\n");
    dynarray_deinit(&list);
}
void slnode_main()
{
    struct SLNode *head = NULL;
    printf("Push_front value -0.89\n");
    head = slnode_push_front(head, -0.890);
    slnode_print(head);
    printf("Push_back value 7.2\n");
    head = slnode_push_back(head, 7.200);
    slnode_print(head);
    printf("Size:%zu\n", slnode_size(head));
    printf("Sum:%f\n", slnode_sum(head));
    printf("Positive:%zu\n", slnode_count_pos(head));
    printf("Clear:\n");
    slnode_clear(head);
}
int main()
{
    char command[100];
    printf("Please, enter command:\n");
    fgets(command, 100, stdin);
    if ((strncmp(command, "dynarray_init", 13) == 0) && (strlen(command) == 14))
    {
        struct Dynarray list;
        dynarray_init(&list);
        char command[100];
        fgets(command, 100, stdin);
        while (strncmp(command, "dynarray_deinit", 15) != 0 || strlen(command) != 16)
        {
            if (strncmp(command, "dynarray_clear", 14) == 0 && strlen(command) == 15)
            {
                dynarray_clear(&list);
            }
            else if (strncmp(command, "dynarray_pop_front", 18) == 0 && strlen(command) == 19)
            {
                dynarray_pop_front(&list);
            }
            else
            {
                if (strncmp(command, "dynarray_resize_", 16) == 0)
                {
                    char *l = command + 16;
                    if (*l == '\n')
                    {
                        printf("You forgot to enter lenght and value\n");
                    }
                    else
                    {
                        char *next = NULL;
                        int new_len = strtol(l, &next, 10);
                        if (next == l)
                        {
                            printf("Incorrect new lenght\n");
                        }
                        else
                        {
                            if (*next == '\n')
                            {
                                printf("You forgot to enter value\n");
                            }
                            else
                            {
                                if (*next == '_')
                                {
                                    char *next2 = NULL;
                                    int value = strtol(next + 1, &next2, 10);
                                    if (next2 == next + 1)
                                    {
                                        printf("Incorrect value\n");
                                    }
                                    else
                                    {
                                        dynarray_resize(&list, new_len, value);
                                    }
                                }
                                else
                                {
                                    printf("Incorrect command\n");
                                }
                            }
                        }
                    }
                }
                else if (strncmp(command, "dynarray_insert_", 16) == 0)
                {
                    char *index = command + 16;
                    if (*index == '\n')
                    {
                        printf("You forgot to enter index and value\n");
                    }
                    else
                    {

                        char *next = NULL;
                        int new_index = strtol(index, &next, 10);
                        if (next == index || new_index > list.lenght)
                        {
                            printf("Incorrect new index\n");
                        }
                        else
                        {
                            if (*next == '\n')
                            {
                                printf("You forgot to enter value\n");
                            }
                            else
                            {
                                if (*next == '_')
                                {
                                    char *next2 = NULL;
                                    int value = strtol(next + 1, &next2, 10);
                                    if (next2 == next + 1)
                                    {
                                        printf("Incorrect value\n");
                                    }
                                    else
                                    {
                                        dynarray_insert(&list, new_index, value);
                                    }
                                }
                                else
                                {
                                    printf("Incorrect command\n");
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (strncmp(command, "dynarray_print", 14) == 0 && strlen(command) == 15)
                    {
                        dynarray_print(&list);
                    }
                    else
                    {
                        printf("Incorrect command\n");
                    }
                }
            }
            fgets(command, 100, stdin);
        }
        dynarray_deinit(&list);
    }
    else if (strncmp(command, "slnode_", 7) == 0)
    {
        struct SLNode *head = NULL;
        if (strncmp(command, "slnode_main", 11) == 0 && strlen(command) == 12)
        {
            slnode_main();
        }
        else
        {
            while (strncmp(command, "slnode_clear", 12) != 0 || strlen(command) != 13)
            {
                if (strncmp(command, "slnode_size", 11) == 0 && strlen(command) == 12)
                {
                    size_t size = slnode_size(head);
                    printf("Size:%zu\n", size);
                }
                else if (strncmp(command, "slnode_count_pos", 16) == 0 && strlen(command) == 17)
                {
                    size_t pos = slnode_count_pos(head);
                    printf("Positive:%zu\n", pos);
                }
                else
                {
                    if (strncmp(command, "slnode_sum", 10) == 0 && strlen(command) == 11)
                    {
                        float sum = slnode_sum(head);
                        printf("Sum:%f\n", sum);
                    }
                    else if (strncmp(command, "slnode_push_front_", 18) == 0)
                    {
                        char *value = command + 18;
                        if (*value == '\n')
                        {
                            printf("You forgot to enter value\n");
                        }
                        else
                        {
                            char *next = NULL;
                            float new_value = strtod(value, &next);
                            if (next == value)
                            {
                                printf("Incorrect value\n");
                            }
                            else
                            {
                                head = slnode_push_front(head, new_value);
                            }
                        }
                    }
                    else
                    {
                        if (strncmp(command, "slnode_push_back_", 17) == 0)
                        {
                            char *value = command + 17;
                            if (*value == '\n')
                            {
                                printf("You forgot to enter value\n");
                            }
                            else
                            {
                                char *next = NULL;
                                float new_value = strtod(value, &next);
                                if (next == value)
                                {
                                    printf("Incorrect value\n");
                                }
                                else
                                {
                                    head = slnode_push_back(head, new_value);
                                }
                            }
                        }
                        else if (strncmp(command, "slnode_print", 12) == 0 && strlen(command) == 13)
                        {
                            slnode_print(head);
                        }
                        else
                        {
                            printf("Incorrect command\n");
                        }
                    }
                }

                fgets(command, 100, stdin);
            }
            slnode_clear(head);
        }
    }
    else
    {
        if (strncmp(command, "dynarray_main", 13) == 0 && strlen(command) == 14)
        {
            dynarray_main();
        }
        else
        {
            printf("Incorrect command\n");
        }
    }
}