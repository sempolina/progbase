#include <stdio.h>
#include <math.h>
#include <stdbool.h>
double fy(double x)
{
    double y=0;
   if((x>=-4.5&&x<-1)||(x>1&&x<=4.5))
    {
        y=(tan(x+2))+2;
    }
    else
    {
        y=pow((x+3),3)+1/x;
    } 
    return y;
}
double int_fy(double x_min,double x_max, double x_step)
{
    double integ=0;
    for(double i=x_max;i>x_min;i=i-x_step) integ=integ+fy(i);
    integ=integ*x_step;
    return integ;
}
int main()
{
    bool a=true;
    double xmin=0,xmax=0,xstep=0,pi=3.141592,cons=(pi-4)/2,integ=0;
    printf("Enter xmin:");
    scanf("%lf",&xmin);
    printf("Enter xmax:");
    scanf("%lf",&xmax);
    printf("Enter xstep:");
    scanf("%lf",&xstep);
    if(xmin>=xmax)
    {
     a=false;
    }
    else 
    {
        if((xmin<=0)&&(xmax>=0))
        {
            a=false;
        }
       if((((cons-pi)>=xmin)&&((cons-pi)<=xmax))||(((cons+pi)>=xmin)&&((cons+pi)<=xmax)))
       {
           a=false;
       }     
    }
  if(a==false) printf("Error\n");
  else
  {
   integ=int_fy(xmin,xmax,xstep);
   printf("Integral:%lf\n",integ);
  }
}