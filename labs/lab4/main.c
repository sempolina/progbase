#include<stdio.h>
#include<math.h>
#include<time.h>
#include<stdlib.h>
double fy(double x)
{
   double y=0;
  if((x>=-4.5&&x<-1)||(x>1&&x<=4.5))
   {
       y=(tan(x+2))+2;
   }
   else
   {
       y=pow((x+3),3)+1/x;
   }
   return y;
}
void Int_array()
{
   srand(time(0));
   int A[30]={0};
   int B[30]={0};
   printf("A= ");
   for (int i=0;i<30;i++)
   {
    A[i]=rand()%21+(-10);
    printf("%i ",A[i]);
   }
   printf("\nB= ");
   for(int i=0;i<29;i++)
   {
    B[i]=A[i]-A[i+1];
    printf("%i ",B[i]);
   }
   B[29]=A[29]-A[0];
   printf("%i\n",B[29]);
}
void Float_matrix(double x_min,double x_max)
{
double x_step=(x_max-x_min)/20;
double M[2][20]={0};
for(int i=0;i<20;i++)
{
   M[0][i]=x_min+i*x_step;
}
double pi=3.141592,cons=(pi-4)/2;
for(int i=0;i<20;i++)
{
  if((M[0][i]==0)||(M[0][i]==(cons-pi))||(M[0][i]==(cons+pi)))
   {
       printf("This matrix doesn`t exist.\n");
       return ;
   }
   M[1][i]=fy(M[0][i]);
}
printf("M:\n");
for(int i=0;i<20;i++) printf("%lf ",M[0][i]);
printf("\n");
for(int i=0;i<20;i++) printf("%lf ",M[1][i]);
 
}
int main()
{
   int opt=0;
   printf("Please, choose option:\n1. Int array\n2. Float matrix\n3. Quit");
   scanf("%i",&opt);
   while(opt!=3)
   {
    if(opt!=1&&opt!=2) printf("This option doesn`t exist. Choose option again, please\n");
    else if(opt==1)
    {
      Int_array();
    }
    else
    {
      double x_min=0,x_max=0;
      printf("Please, enter x_min and x_max");
      scanf("%lf%lf",&x_min, &x_max);
      while(x_min>=x_max)
      {
        printf("x_min can`t be bigger or equivalent than x_max. Please, enter x_min and x_max again");
        scanf("%lf%lf",&x_min, &x_max);
      }
      Float_matrix(x_min,x_max);
    }
    printf("Please, choose option:\n1. Int array\n2. Float matrix\n3. Quit");
    scanf("%i",&opt);
   }
   printf("Good bye!\n");
}
